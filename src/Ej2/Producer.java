package Ej2;

import java.util.Random;

public class Producer {
    private MyBuffer mybuffer;
    private int id;

    public Producer(MyBuffer mybuffer, int id) {
        this.mybuffer = mybuffer;
        this.id = id;
    }

    public void run() {

        while(true){
            Random claseRandom = new Random();
            int i = 15 + claseRandom.nextInt(0 - 9);

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Producer " + claseRandom + " put: " + i);
        }
    }
}
