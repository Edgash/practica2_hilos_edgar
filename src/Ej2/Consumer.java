package Ej2;

public class Consumer implements Runnable {
    private int id;
    private int valor;
    private MyBuffer buffer;

    public Consumer(int id, MyBuffer buffer) {
        this.id = id;
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while (true){
            try {
                valor = buffer.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Consumer" + this.id + " get " + valor);
        }
    }
}
