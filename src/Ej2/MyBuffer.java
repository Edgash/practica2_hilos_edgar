package Ej2;

public class MyBuffer {
    private boolean full;
    private int buffer = 0;

    public synchronized void put(int i) throws InterruptedException {
        while (full){
            wait();
        }
        buffer = i;
        full = true;
        notify();
    }

    public synchronized int get() throws InterruptedException {
        while (!full){
            wait();
        }
        full = false;
        notifyAll();
        return buffer;
    }
}
