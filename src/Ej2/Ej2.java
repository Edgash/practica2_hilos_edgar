package Ej2;

public class Ej2 {

    public static void main(String[] args) {
        int QUEUE_SIZE = 5;
        int MAX_WRITTEN_MESSAGES = 1;
        int NUM_READERS = 3;
        int NUM_WRITERS = 5;
        Thread[] producer = new Thread[NUM_WRITERS];
        Thread[] consumer = new Thread[NUM_READERS];
        MyBuffer buffer = new MyBuffer();

        for(int i = 0; i < NUM_WRITERS;i++){
            //producer[i] = new Thread(new Producer(i, buffer, QUEUE_SIZE));
            producer[i].start();
        }

    }

}
