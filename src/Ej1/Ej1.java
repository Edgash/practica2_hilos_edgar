package Ej1;

import java.util.Scanner;

public class Ej1 {

    static int totalSumValue;
    static int averageValue;

    public static void main(String[] args) throws InterruptedException {
        int n;
        //pregunta al usuario la cantidad de hilos que desea
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("¿Cuántos hilos se van a ejecutar?");
            n = sc.nextInt();
        }while(n < 50 || n > 100);
        //pregunta al usuario el número a operar
        Scanner sc = new Scanner(System.in);
        System.out.println("Número a calcular");
        int valor = sc.nextInt();

        Calculate calculate = new Calculate(valor);

        //crea los hilos y los inicia
       Thread[] calcular = new Thread[n];
        for (int i = 0; i < n; i++) {
            calcular[i] = new Thread(calculate);
            calcular[i].start();
        }
        for (int i = 0; i < n; i++) {
            calcular[i].join();
        }

        averageValue = totalSumValue / valor;

        System.out.println("Suma total " + totalSumValue + "\nPromedio total " + averageValue);
    }

}
