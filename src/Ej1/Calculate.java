package Ej1;

public class Calculate implements Runnable{

        //el constructor para poder introducir el número al hilo
        int n;
        Calculate(int n){
            this.n = n;
        }
        //el metodo syncronized que sumará los números
        public synchronized int newValue(int num){

            Ej1.totalSumValue = Ej1.totalSumValue + num;

            return Ej1.totalSumValue;
        }

        @Override
        public void run() {
            newValue(n);
        }



    }
